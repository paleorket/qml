QT += testlib
QT -= gui

TARGET = client-tests
TEMPLATE = app

CONFIG += c++14
CONFIG += console
CONFIG -= app_bundle

include(../qmake-target-platform.pri)
include(../qmake-destination-path.pri)

INCLUDEPATH += source \
            ../cm-lib/source

SOURCES += source/models/client-tests.cpp \
    source/controllers/mastercontrollertests.cpp \
    source/data/datetimedecoratortests.cpp \
    source/data/enumeratordecoratortests.cpp \
    source/data/intdecoratortests.cpp \
    source/data/stringdecoratortests.cpp \
    source/main.cpp \
    source/testsuite.cpp

LIBS += -L$$PWD/../binaries/$$DESTINATION_PATH -lcm-lib

DESTDIR = $$PWD/../binaries/$$DESTINATION_PATH
OBJECTS_DIR = $$PWD/build/$$DESTINATION_PATH/.obj
MOC_DIR = $$PWD/build/$$DESTINATION_PATH/.moc
RCC_DIR = $$PWD/build/$$DESTINATION_PATH/.qrc
UI_DIR = $$PWD/build/$$DESTINATION_PATH/.ui

HEADERS += \
    source/controllers/mastercontrollertests.h \
    source/data/datetimedecoratortests.h \
    source/data/enumeratordecoratortests.h \
    source/data/intdecoratortests.h \
    source/data/stringdecoratortests.h \
    source/models/client-tests.h \
    source/testsuite.h
