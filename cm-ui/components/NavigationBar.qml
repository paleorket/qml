import QtQuick 2.9
import assets 1.0

Item {
    property bool isCollapsed: true

    anchors {
        top: parent.top
        bottom: parent.bottom
        left: parent.left
    }
    width: isCollapsed ? Style.widthNavigationBarCollapsed : Style.heightNavigationBarExpanded

    Rectangle {
        anchors.fill: parent
        color: Style.colourNavigationBarBackground

        Column {
            width: parent.width

            NavigationButton {
                iconCharacter: "\uf0c9"
                description: ""
                hoverColour: "#003355"
                onNavigationButtonClicked: isCollapsed = !isCollapsed
            }
            NavigationButton {
                iconCharacter: "\uf015"
                description: qsTr("Dashboard")
                hoverColour: "#003355"
                onNavigationButtonClicked: masterController.ui_navigationController.goDashboardView();
            }
            NavigationButton {
                iconCharacter: "\uf234"
                description: qsTr("New Client")
                hoverColour: "#003355"
                onNavigationButtonClicked: masterController.ui_navigationController.goCreateClientView();
            }
            NavigationButton {
                iconCharacter: "\uf002"
                description: qsTr("Find Client")
                hoverColour: "#003355"
                onNavigationButtonClicked: masterController.ui_navigationController.goFindClientView();
            }
            NavigationButton {
                iconCharacter: "\uf09e"
                description: qsTr("RSS Feed")
                hoverColour: "#003355"
                onNavigationButtonClicked: masterController.ui_navigationController.goRssView();
            }
        }
    }
}
