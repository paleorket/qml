QT -= gui
QT += sql network xml

TARGET = cm-lib
TEMPLATE = lib

CONFIG += c++14

DEFINES += CMLIB_LIBRARY

include(../qmake-target-platform.pri)
include(../qmake-destination-path.pri)

INCLUDEPATH += source

SOURCES += source/models/client.cpp \
    source/controllers/commandcontroller.cpp \
    source/controllers/databasecontroller.cpp \
    source/controllers/master-controller.cpp \
    source/data/datadecorator.cpp \
    source/data/datetimedecorator.cpp \
    source/data/entity.cpp \
    source/data/enumeratordecorator.cpp \
    source/data/intdecorator.cpp \
    source/data/stringdecorator.cpp \
    source/framework/command.cpp \
    source/models/address.cpp \
    source/models/appointment.cpp \
    source/models/clientsearch.cpp \
    source/models/contact.cpp \
    source/networking/networkaccessmanager.cpp \
    source/networking/webrequest.cpp \
    source/rss/rsschannel.cpp \
    source/rss/rssimage.cpp \
    source/rss/rssitem.cpp \
    source/utilities/xmlhelper.cpp

HEADERS += source/cm-lib_global.h \
    source/controllers/commandcontroller.h \
    source/controllers/databasecontroller.h \
    source/controllers/i-database-controller.h \
    source/data/datadecorator.h \
    source/data/datetimedecorator.h \
    source/data/entity-collection.h \
    source/data/entity.h \
    source/data/enumeratordecorator.h \
    source/data/intdecorator.h \
    source/data/stringdecorator.h \
    source/framework/command.h \
    source/models/address.h \
    source/models/appointment.h \
    source/models/client.h \
    source/controllers/master-controller.h \
    source/controllers/navigation-controller.h \
    source/models/clientsearch.h \
    source/models/contact.h \
    source/networking/i-network-access-manager.h \
    source/networking/i-web-request.h \
    source/networking/networkaccessmanager.h \
    source/networking/webrequest.h \
    source/rss/rsschannel.h \
    source/rss/rssimage.h \
    source/rss/rssitem.h \
    source/utilities/xmlhelper.h

message(cm-lib project dir: $${PWD})

DESTDIR = $$PWD/../binaries/$$DESTINATION_PATH
OBJECTS_DIR = $$PWD/build/$$DESTINATION_PATH/.obj
MOC_DIR = $$PWD/build/$$DESTINATION_PATH/.moc
RCC_DIR = $$PWD/build/$$DESTINATION_PATH/.qrc
UI_DIR = $$PWD/build/$$DESTINATION_PATH/.ui
message(cm-lib output dir: $${DESTDIR})
