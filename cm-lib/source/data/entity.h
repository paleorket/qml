#ifndef ENTITY_H
#define ENTITY_H

#include <map>

#include <QObject>
#include <QScopedPointer>

#include <cm-lib_global.h>
#include <data/datadecorator.h>
#include <data/entity-collection.h>
#include <data/stringdecorator.h>

namespace cm {
namespace data {

class CMLIBSHARED_EXPORT Entity : public QObject
{
    Q_OBJECT
public:
    explicit Entity(QObject *parent = nullptr, const QString& key = "SomeItemKey");
    explicit Entity(QObject* parent, const QString& key, const QJsonObject& jsonObject);
    virtual ~Entity();

public:
    const QString& key() const;
    const QString& id() const;
    void update(const QJsonObject& jsonObject);
    QJsonObject toJson() const;

signals:
    void childEntitiesChanged();
    void dataDecoratorChanged();
    void childCollectionChanged(const QString& collectionKey);

protected:
    Entity* addChild(Entity* entity, const QString& key);
    DataDecorator* addDataItem(DataDecorator* dataDecorator);
    EntityCollectionBase* addChildCollection(EntityCollectionBase* entityCollection);
    void setPrimaryKey(StringDecorator* primaryKey);

protected:
    class Implementation;
    QScopedPointer<Implementation> implementation;
};

}
}

#endif // ENTITY_H
